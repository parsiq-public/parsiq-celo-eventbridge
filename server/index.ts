import path from "path";
import express from "express";
import http from "http";
import IO from "socket.io";
import { onShutdown } from "node-graceful-shutdown";
import { createHttpTerminator } from "http-terminator";
import { initCelo, buildHistory } from "./rosetta";
import { BlockIdentifier, BlockHashIndexIdentifier } from "./rosetta-types";
import * as amqp from "amqp-connection-manager";
 

const app = express();
const httpServer = http.createServer(app);
const io = IO(httpServer);
const NOBODY = "0x0000000000000000000000000000000000000000";

/////////////////////// Environment checks ////////////////////////////

function getEnv(name: string) {
  let value = process.env[name];
  if (!value) {
    console.error(`Missing ${name} environment variable`);
    process.exit(-1);
  }
  return value;
}

const GETH_RPC = getEnv("GETH_RPC");
const ROSETTA_RPC = getEnv("ROSETTA_RPC");
const CELO_DATA_DIR = getEnv("CELO_DATA_DIR");

const RMQ_USER = getEnv("RMQ_USER");
const RMQ_PASSWORD = getEnv("RMQ_PASSWORD");
const RMQ_SERVER = getEnv("RMQ_SERVER");

const QUEUE_NAME = 'celo.mainnet';


///////////////////////////// Static Assets ///////////////////////////

app.use(express.static(path.join(__dirname, '../build')));
app.get('/', (req, res) => res.sendFile(__dirname + '/index.html'));


////////////////////// Real-time Communication ////////////////////////

let clients = 0;
const PROTOCOL_VERSION = 3; // will force clients to reload if changed

async function main() {

    const ctx = await initCelo(GETH_RPC, ROSETTA_RPC, CELO_DATA_DIR);
    const {rosetta, network_identifier, history, web3, cUSD, cUSDCurrency} = ctx;

    // Create a new connection manager
    let connection = amqp.connect([`amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_SERVER}`]);
    
    // Ask the connection manager for a ChannelWrapper.  Specify a setup function to
    // run every time we reconnect to the broker.
    var channelWrapper = connection.createChannel({
        json: true,
        setup: function(channel: any) {
            // `channel` here is a regular amqplib `ConfirmChannel`.
            // Note that `this` here is the channelWrapper instance.
            return channel.assertQueue(QUEUE_NAME, {durable: true});
        }
    });

    async function traceBlock(block_identifier: BlockIdentifier) {
      const block = await rosetta.block({
        block_identifier,
        network_identifier
      });
      const id = block.block.block_identifier as BlockHashIndexIdentifier
      let txs = block.other_transactions || [];
      let txObj = {} as any;
      const result = {
        id,
        tx: txObj,
        txids:[] as string[]
      }
      if (txs.length > 0) {
        for(const tx of txs) {
          try {
            let txdata = (await rosetta.blockTransaction({
              block_identifier: id,
              network_identifier,
              transaction_identifier: tx
            })).transaction;
            txdata.block_identifier = id;
            txdata.related_operations = txdata.related_operations || [];
            txObj[tx.hash] = txdata;

          } catch (e) {
            console.log(e)
          }
        }
        history[id.index] = result
        let tokenTransfers = await cUSD.getPastEvents("Transfer", {fromBlock:id.index, toBlock:id.index});
        for (const t of tokenTransfers) {
          try {
            let tx = txObj[t.transactionHash];
            let one = {index: tx.operations.length }
            let two = {index: tx.operations.length + 1 }
            let operation_from = {
              operation_identifier: one,
              related_operations: [two],
              type: "transfer",
              account: { address: t.returnValues.from },
              status: "success",
              amount: {
                value: "-" + t.returnValues.value,
                currency: cUSDCurrency
              }              
            }
            let operation_to = {
              operation_identifier: two,
              related_operations: [one],
              type: "transfer",
              account: { address: t.returnValues.to },
              status: "success",
              amount: {
                value: t.returnValues.value,
                currency: cUSDCurrency
              }              
            }
            if (operation_from.account.address === NOBODY) {
              operation_to.related_operations = []
              operation_to.operation_identifier = one;
              operation_to.type = "mint";
              tx.operations.push(operation_to);              
            } else if (operation_to.account.address === NOBODY) {
              operation_from.related_operations = []
              operation_from.operation_identifier = one;
              operation_to.type = "burn";
              tx.operations.push(operation_from);
            } else {
              tx.operations.push(operation_from, operation_to);
            }
          } catch (e) {
            console.error(e);
          }
        }
        result.txids = txs.map((tx:{hash: string}) => tx.hash)
        await pushBlockToRMQ(result)
      } else {
        result.txids = []
      }
      return result;
    }

    async function pushBlockToRMQ(block: any) {
      for(const txid of block.txids) {
        await pushTxToRMQ(block.tx[txid]);
      }
    }

    async function pushTxToRMQ(tx:any) {
      const serialized = {
        txHash: tx.transaction_identifier.hash,
        block: tx.block_identifier,
        operations: tx.operations.map((op: any) => ({
          type: op.type,
          status: op.status,
          account: op.account.address,
          subAccount: op.account.subAccount?.address ?? "",
          amount: {
            value: op.amount?.value ?? "0",
            currency: op.amount?.currency ?? { symbol: "", decimals: 0 }
          },
          refs: op.related_operations?.map((ro:any) => ro.index) ?? []
        }))
      }

      for (const op of serialized.operations) {
        const opEvent = {
          pattern: "celoBalanceChange",
          data: {...op, ...serialized}
        }
        try {
          await pushToRMQ(opEvent);
        } catch (e) {
          console.error(e);
        }
      }
    }

    async function pushToRMQ(obj: any) {
      // Send some messages to the queue.  If we're not currently connected, these will be queued up in memory
      // until we connect.  Note that `sendToQueue()` and `publish()` return a Promise which is fulfilled or rejected
      // when the message is actually sent (or not sent.)
      await channelWrapper.sendToQueue(QUEUE_NAME, obj)
    }

    buildHistory(ctx, traceBlock).catch(console.error).then(async () => {
      io.emit("history", history);
    }); // run in parallel;

    io.on('connect', (socket) => {
      console.log(`Client connected (total: ${++clients})`);
      socket.emit('version', PROTOCOL_VERSION);
      socket.emit("history", history);
    
      socket.on('disconnect', () => {
        console.log(`Client disconnected (total: ${--clients})`);
      })
    });    

    
    var subscription = web3.eth.subscribe('newBlockHeaders')
      .on("data", async function(blockHeader){
        try {
          let block = await traceBlock({hash: blockHeader.hash});
          io.emit('message', block);
        } catch (e) {
          console.error(e)
        }
      })
      .on("error", console.error);

    ////////////////////// Listening /////////////////////////////////////

    const port = process.env.PLAYGROUND_PORT || 3001;
    app.set('port', port);




    const server = httpServer.listen(port, () => {
      console.log('= Celo-PARSIQ playground =');
      console.log(`Server started at *:${port}`);
      console.log(`Protocol version: ${PROTOCOL_VERSION}`);
    });


    //////////////////// Graceful termination ///////////////////////////

    const httpTerminator = createHttpTerminator({server});

    onShutdown('notify', async () => {
      console.log('');
      console.log('Received shutdown signal');
    })

    onShutdown('broadcasting', ['notify'],  async() => {
      console.log('Stop broadcasting');
      const success = await new Promise((resolve, reject) => {
        subscription.unsubscribe((err, res) => err ? reject(err) : resolve(res))
      });
      if (success) {
          console.log('Successfully unsubscribed from Celo-node!');
      }
    });

    onShutdown('http-server', ['broadcasting'], async () => {
      console.log('shutting down playground');
      await httpTerminator.terminate()
    });

}

main().catch(console.error);
