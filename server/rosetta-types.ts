export type NetworkIdentifier = {
	network: string,
	blockchain: string
}

export type BlockIndexIdentifier = { index: number }
export type BlockHashIdentifier = { hash: string }
export type BlockHashIndexIdentifier = BlockIndexIdentifier & BlockHashIdentifier
export type BlockIdentifier = BlockIndexIdentifier 
					 | BlockHashIdentifier 
					 | BlockHashIndexIdentifier
export type TransactionIdentifier = { hash: string }
export type HasNetworkIdentifier = { network_identifier: NetworkIdentifier }
export type NetworkListRequest = {}
export type NetworkListResponse = { network_identifiers: NetworkIdentifier[] }
export type NetworkStatusRequest = {} & HasNetworkIdentifier
export type NetworkStatusResponse = {
	current_block_identifier: BlockHashIndexIdentifier
}
export type NetworkOptionsRequest = {} & HasNetworkIdentifier
export type BlockRequest = {
	block_identifier: BlockIdentifier
} & HasNetworkIdentifier
export type BlockResponse = any // TODO: Add Strong Typing

export type BlockTransactionRequest = {
	block_identifier: BlockHashIndexIdentifier
	transaction_identifier: TransactionIdentifier
} & HasNetworkIdentifier

export type BlockTransactionResponse = any // TODO: Add strong typing


type Async<TIn, TOut> = (args: TIn) => Promise<TOut>

export const RosettaAPI = { 
    networkList: "/network/list",
    networkStatus: "/network/status",
	block: "/block",
	blockTransaction: "/block/transaction"
}

export type RosettaAPI = {
    networkList: Async<NetworkListRequest, NetworkListResponse>,
    block: Async<BlockRequest, BlockResponse>,
	networkStatus: Async<NetworkStatusRequest, NetworkStatusResponse>,
	blockTransaction: Async<BlockTransactionRequest, BlockTransactionResponse>
}

type Check1 = {[P in keyof typeof RosettaAPI]: RosettaAPI[P]}
type Check2 = {[P in keyof RosettaAPI]: (typeof RosettaAPI)[P]}
