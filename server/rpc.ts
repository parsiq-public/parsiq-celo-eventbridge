import fetch from "node-fetch";

export type PostFn = <TIn, TOut>(route: string, body: TIn) => Promise<TOut>
export const mkPostFn = (url: string) => 
    <TIn, TOut>(route: string, body: TIn) => 
        fetch(`${url}${route}`, {
            method: 'post',
            body: JSON.stringify(body),
            headers: { 'Content-Type': 'application/json'}
        }).then(res => res.json()) as Promise<TOut>;

export type Sequence = () => number;
export const mkSequence = (value: number) => () => value++;
        
export type JsonRpcBody<TParams extends Array<unknown>> = {
    jsonrpc: string, 
    method: string,
    params: TParams,
    id: number
}

export const jsonrpc = "2.0";
export const mkJsonFn = (post: PostFn, mkId: Sequence) => 
    <TParams extends Array<unknown>, TOut>(
        method: string, 
        ...params: TParams
    ) => post<JsonRpcBody<TParams>, {jsonrpc: string, id: number, result: TOut}>("/", { 
        jsonrpc, 
        method, 
        params, 
        id: mkId() 
    })

export const mkCall = (post: PostFn) => 
    <TIn, TOut>(route: string) => 
        (args: TIn) => 
            post(route, args) as Promise<TOut>

const define = Object.defineProperty

export const mkAPI = (post: PostFn, routes: {[index: string]: string}) => {
    const api: {[index: string]: Function}  = {}
    for(const [method, route] of Object.entries(routes)) {
        api[method] = (args: any) => post(route as string, args)
    }
    return api;
}




